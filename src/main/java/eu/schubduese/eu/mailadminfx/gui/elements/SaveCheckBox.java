package eu.schubduese.eu.mailadminfx.gui.elements;

import eu.schubduese.eu.mailadminfx.config.Settings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;

public class SaveCheckBox extends CheckBox
{
	
	public SaveCheckBox()
	{
		super();
	}
	
	
	public SaveCheckBox(String value)
	{
		super();
		init(value);
	}
	
	public void init(String value)
	{
		setSelected(Settings.loadBoolValue(value));
		selectedProperty().addListener(new ChangeListener<Boolean>() 
		{
			
		    @Override
		    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
		    {
		        Settings.setBoolValue(value, isSelected());
		    }
		    
		});

	}
}
