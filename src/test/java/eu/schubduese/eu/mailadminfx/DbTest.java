package eu.schubduese.eu.mailadminfx;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

public class DbTest 
{
	private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String DB_USER = "";
    private static final String DB_PASSWORD = "";
    
    private Connection connection;
	
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void insertDomain()
    {
        
    	assertTrue( true );
    }
    
    @Before
    public void  generateTables()
    {
    	ClassLoader classLoader = getClass().getClassLoader();
    	File file = new File(classLoader.getResource("installTables.sql").getFile());
    	byte[] encoded;
    	String sqlCommand = null;
		try {
			encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
			sqlCommand = new String(encoded, "UTF-8");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	try {
			getDBConnection().prepareCall(sqlCommand);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    private Connection getDBConnection() 
    {
    	if (connection == null)
    	{
    		try {
    			Class.forName(DB_DRIVER);
    		} catch (ClassNotFoundException e) {
    			System.out.println(e.getMessage());
    		}
    		try {
    			connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
    			return connection;
    		} catch (SQLException e) {
    			System.out.println(e.getMessage());
    		}
    		
    	}
        return connection;
    }
}
