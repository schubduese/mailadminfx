package eu.schubduese.eu.mailadminfx.utils;

import javafx.scene.image.Image;

public class FxImageTools
{
	
	FxImageTools fxImageTools;
	
	public FxImageTools()
	{
		super();
	}
	
	public Image loadImage(String filename)
	{
		Image image = new Image(getClass().getResourceAsStream("/images/" + filename));
		return image;
	}
	

}
