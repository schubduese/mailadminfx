package eu.schubduese.eu.mailadminfx.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Properties;

import eu.schubduese.eu.mailadminfx.utils.StringUtils;


public class Settings
{

	static Properties properties;
	private static File propertiesFile;
	
	public enum PropKey
	{

		FRITZBOXUSER("fritzboxuser"),
		FRITZBOXPASSWORT("fritzboxpasswort"),
		FRITZBOXIP("fritzboxip"),
		FRITZBOXPORT("fritzboxport"),
		FRITZBOXAUTOCONNECT("fritzboxautoconnect"),
		FRITZBOXACTRATE("fritzboxactrate");

		private String propertiesValue;

		PropKey(String propertiesValue)
		{
			this.propertiesValue = propertiesValue;
		}

		public String getPropertiesValue()
		{
			return propertiesValue;
		}

	}

	public static String loadValue(String cnfKey, String defaultValue)
	{
		String s = loadValue(cnfKey);
		if (StringUtils.isEmpty(s))
			return defaultValue;
		else
			return s;
	}
	
	public static String loadValue(String cnfKey)
	{
		return getProperties().getProperty(cnfKey);
	}
	
	
	public static int loadIntValue(String cnfKey, int defaultValue)
	{
		int i = loadIntValue(cnfKey);
		if (i == -1)
			return defaultValue;
		else
			return i;
	}
	
	public static boolean loadBoolValue(String cnfKey, boolean defaultValue)
	{
		int i = loadIntValue(cnfKey);
		if (i == -1)
			return defaultValue;
		else 
			return i != 0;
	}
	
	public static boolean loadBoolValue(String cnfKey)
	{
		int i = loadIntValue(cnfKey);
		return i == 1;
	}
	
	public static int loadIntValue(String cnfKey)
	{
		String value = getProperties().getProperty(cnfKey);
		if (StringUtils.isNotEmpty(value))
		{
			try
			{
				return Integer.parseInt(value);
			}
			catch (NumberFormatException e) 
			{
				//hier nichts machen
			}
		}
		return -1;
	}
	
	public static void setBoolValue(String cnfKey, boolean value)
	{
		setValue(cnfKey, value ? "1": "0");
	}

	public static void setValue(String cnfKey, String value)
	{
		if (cnfKey != null && StringUtils.isNotEmpty(value))
			getProperties().setProperty(cnfKey, value);
	}

	private static Properties getProperties()
	{
		if (properties == null)
		{
			try
			{
				FileReader reader = new FileReader(getPropertiesFile());
				properties = new Properties();
				properties.load(reader);
				// String host = props.getProperty("host");
				//
				// System.out.print("Host name is: " + host);
				reader.close();
			} catch (FileNotFoundException ex)
			{
				// file does not exist
			} catch (IOException ex)
			{
				// I/O error
			}

		}
		return properties;
	}

	public static boolean save()
	{
		try
		{
			OutputStream outputStream = new FileOutputStream(getPropertiesFile());
			Writer writer = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
			getProperties().store(writer, "");
			writer.close();
			return true;
		} catch (FileNotFoundException ex)
		{
			// file does not exist
		} catch (IOException ex)
		{
			// I/O error
		}
		return false;
	}
	
	private static File getPropertiesFile()
	{
		if (propertiesFile == null)
		{
			propertiesFile = new File(System.getProperty("user.home") + "/" + ".mailadminfx");
			if (!propertiesFile.exists())
			{
				try
				{
					propertiesFile.createNewFile();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
				
			}
		}
		return propertiesFile;
	}

}
