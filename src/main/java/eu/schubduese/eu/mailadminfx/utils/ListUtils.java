package eu.schubduese.eu.mailadminfx.utils;

import java.util.List;

public class ListUtils
{

	public static boolean isNotEmpty(List<?> liste)
	{
		return !ListUtils.isEmpty(liste);
	}

	public static boolean isEmpty(List<?> liste)
	{
		if (liste == null || liste.size() == 0)
			return true;
		return false;
	}

}
