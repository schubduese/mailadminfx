package eu.schubduese.eu.mailadminfx.utils;

public class StringUtils
{

	public static boolean isEmpty(CharSequence text)
	{
		if (text == null || text.length() == 0)
			return true;
		int len = text.length();
		for (int i = 0; i < len; i++)
		{
			if (text.charAt(i) != ' ')
				return false;
		}
		return true;
	}

	public static boolean isNotEmpty(CharSequence text)
	{
		return !isEmpty(text);
	}
}
