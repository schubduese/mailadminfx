package eu.schubduese.eu.mailadminfx.gui;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class GuiTools
{

	public static void showMessageDialog(String title, String message)
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(message);
		alert.showAndWait();
	}
}
