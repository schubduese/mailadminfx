package eu.schubduese.eu.mailadminfx.dataobjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "accounts")
public class Account extends SQLEntity {
	
	@Column(name="id", table="accounts")
	private String objectId;
	@Column(name="username", table="accounts")
	private String username;
	@Column(name="domain", table="accounts")
	private String domain;
	@Column(name="password", table="accounts")
	private String password;
	@Column(name="quota", table="accounts")
	private String quota;
	@Column(name="enabled", table="accounts")
	private boolean	isEnabled;
	@Column(name="sendonly", table="accounts")
	private boolean isSendOnly;
	
	public String getDomain() 
	{
		return domain;
	}
	
	public void setDomain(String domain) 
	{
		this.domain = domain;
	}
	
	
	public String getPassword() 
	{
		return password;
	}
	
	
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	public boolean isEnabled() 
	{
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) 
	{
		this.isEnabled = isEnabled;
	}

	public boolean isSendOnly() 
	{
		return isSendOnly;
	}

	public void setSendOnly(boolean isSendOnly) 
	{
		this.isSendOnly = isSendOnly;
	}

	public String getQuota() 
	{
		return quota;
	}

	public void setQuota(String quota) 
	{
		this.quota = quota;
	}

	public String getObjectId() 
	{
		return objectId;
	}

	public void setObjectId(String objectId) 
	{
		this.objectId = objectId;
	}

	public String getUsername() 
	{
		return username;
	}

	public void setUsername(String username) 
	{
		this.username = username;
	}

	
}
