package eu.schubduese.eu.mailadminfx.gui;
	
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application
{
	
	@Override
	public void start(Stage primaryStage) 
	{
		try 
		{
			URL url = getClass().getResource("MainGui.fxml");
			FXMLLoader fxmlLoader = new FXMLLoader(url);
			Node node = fxmlLoader.load();
			MainGuiController mainGuiController= fxmlLoader.<MainGuiController>getController();
			mainGuiController.setPrimaryStage(primaryStage);
			primaryStage.setTitle("MailAdminFx");
			Scene scene = new Scene ((Parent) node); 
			primaryStage.setScene(scene);
//			Image image = FxImageTools.getImage("");
//			if (image != null)
//				stage.getIcons().add(image);
			primaryStage.show();
			primaryStage.setMinWidth(800);
			primaryStage.setMinHeight(660); 
		} catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) 
	{
		launch(args);
	}
	
}
