package eu.schubduese.eu.mailadminfx.dataobjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "aliases")
public class Alias extends SQLEntity {
	
	@Column(name="id", table="aliases")
	private String objectId;
	@Column(name="source_username", table="aliases")
	private String source_username;
	@Column(name="source_domain", table="aliases")
	private String source_domain;
	@Column(name="destination_username", table="aliases")
	private String destination_username;
	@Column(name="destination_domain", table="aliases")
	private String destination_domain;
	@Column(name="enabled", table="aliases")
	private boolean enabled;
	
	public String getObjectId() 
	{
		return objectId;
	}

	public void setObjectId(String objectId) 
	{
		this.objectId = objectId;
	}
	
	public String getSource_username() 
	{
		return source_username;
	}
	
	public void setSource_username(String source_username) 
	{
		this.source_username = source_username;
	}
	
	public String getSource_domain() 
	{
		return source_domain;
	}
	
	public void setSource_domain(String source_domain) 
	{
		this.source_domain = source_domain;
	}
	
	public String getDestination_username() 
	{
		return destination_username;
	}
	
	public void setDestination_username(String destination_username) 
	{
		this.destination_username = destination_username;
	}
	
	public String getDestination_domain() 
	{
		return destination_domain;
	}
	
	public void setDestination_domain(String destination_domain) 
	{
		this.destination_domain = destination_domain;
	}
	public boolean isEnabled() 
	{
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
