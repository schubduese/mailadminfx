package eu.schubduese.eu.mailadminfx.dataobjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "domains")
public class Domain extends SQLEntity {
	
	@Column(name="id", table="accounts")
	private String objectId;
	@Column(name="domain", table="accounts")
	private String domain;

	public String getDomain() 
	{
		return domain;
	}

	public void setDomain(String domain) 
	{
		this.domain = domain;
	}

	public String getObjectId() 
	{
		return objectId;
	}

	public void setObjectId(String objectId) 
	{
		this.objectId = objectId;
	}

	
}
