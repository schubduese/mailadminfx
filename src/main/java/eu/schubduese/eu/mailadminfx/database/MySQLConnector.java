package eu.schubduese.eu.mailadminfx.database;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.schubduese.eu.mailadminfx.dataobjects.Alias;
import eu.schubduese.eu.mailadminfx.dataobjects.Domain;
import eu.schubduese.eu.mailadminfx.dataobjects.Account;



public class MySQLConnector 
{
	
	private Logger log = LoggerFactory.getLogger(MySQLConnector.class);
    private String address;
    private Object port;
    private String database;
    private String user;
    private String password;
    private Connection connection = null;
    
    public MySQLConnector(String address, int port, String database, String user, String password) 
    {
        log.debug("Konstruktor PGConnector aufgerufen");
        this.address = address;
        this.port = port;
        this.database = database;
        this.user = user;
        this.password = password;

        try {

            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your MySQL JDBC Driver? "
                    + "Include in your library path!");
            log.error("MySQL JDBC Treiber nicht gefunden", e);
            e.printStackTrace();
            return;

        }
        log.debug("MySQL JDBC Driver Registered!");
        //String conZeile= "jdbc:mysql://" + address + ":" + port + "/" + database + "," + user + "," + password;
        String conZeile= "jdbc:mysql://" + address + ":" + port + "/" + database + "?user=" + user + "&password=" + password;
        //"jdbc:mysql://localhost/feedback?"+ "user=sqluser&password=sqluserpw"
        log.debug("Versuche zu verbinden mit folgendem String: {}", conZeile);
        System.out.println("Verbinde zu folgendem Server: " + address+ ", port: " + port + ", DB: " + database + ", User: " + user +  ", Passwort: " + password);
        try {

            connection = DriverManager.getConnection(conZeile);

        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            log.error("Connection failed", e);
            e.printStackTrace();
            return;

        }

        if (connection != null) {
            log.debug("You made it, take control your database now!");
        } else {
            log.error("Failed to make connection!");
        }

    }
    
    public void querySqlStatement(String sql) 
    {
        Statement st;
        try 
        {
            st = connection.createStatement();
            st.execute(sql);
            st.close();
            //Wieso wird die Verbindung hier geschlossen?
            connection.close();
        } catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }
    
    public String getInstallScriptString()
    {
    	ClassLoader classLoader = getClass().getClassLoader();
    	File file = new File(classLoader.getResource("installTables.sql").getFile());
    	byte[] byteArray;
    	String sqlCommand = "";
		try 
		{
			byteArray = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
			sqlCommand = new String(byteArray, "UTF-8");
		} catch (IOException e1) 
		{
			e1.printStackTrace();
		}
    	return sqlCommand;
    }
    
	public void insertDomain(String domain) throws SQLException {
		// preparedStatements can use variables and are more efficient
		PreparedStatement preparedStatement;
		preparedStatement = connection
				.prepareStatement("insert into  domains (domain) values (?)");
		// parameters start with 1
		preparedStatement.setString(1, domain);
		preparedStatement.executeUpdate();
	}
    
    public void deleteDomainbyId(int id) throws SQLException {
    	PreparedStatement preparedStatement;
		preparedStatement = connection
				.prepareStatement("DELETE from domains where id= ?;");
		// parameters start with 1
		preparedStatement.setString(1, String.valueOf(id));
		preparedStatement.executeUpdate();
    }
    
    public List<Domain> getAllDomains() throws SQLException {
    	Statement statement = connection.createStatement();
    	ResultSet rs = statement.executeQuery("SELECT * from domains;");
    	List<Domain> domainList = new ArrayList<Domain>();
    	while (rs.next()){
    		Domain domain = new Domain();
    		domain.setDomain(rs.getString("domain"));
    		domain.setId(Integer.parseInt(rs.getString("id")));
    		domainList.add(domain);
    		System.out.println(domain.getId() + " " + domain.getDomain());
    	}
    	return domainList;
    }

    public void insertAlias(String sourceAddress, String destinationAdress) throws SQLException {
		// preparedStatements can use variables and are more efficient
		PreparedStatement preparedStatement;
		preparedStatement = connection
				.prepareStatement("insert into  aliases (source, destination) values (?, ?)");
		// parameters start with 1
		preparedStatement.setString(1, sourceAddress);
		preparedStatement.setString(2, destinationAdress);
		preparedStatement.executeUpdate();
    }
    
    public void deleteAliasById(int id) throws SQLException {
    	PreparedStatement preparedStatement;
		preparedStatement = connection
				.prepareStatement("DELETE from aliases where id= ?;");
		// parameters start with 1
		preparedStatement.setString(1, String.valueOf(id));
		preparedStatement.executeUpdate();
    }
    
    public  List<Alias> getAllAliases() throws SQLException {
    	Statement statement = connection.createStatement();
    	ResultSet rs = statement.executeQuery("SELECT * from aliases;");
    	List<Alias> aliasList = new ArrayList<Alias>();
    	while (rs.next()){
    		Alias alias = new Alias();
//    		alias.setSourceAdress(rs.getString("source"));
//    		alias.setDestinationAdress(rs.getString("destination"));
    		alias.setId(Integer.parseInt(rs.getString("id")));
    		aliasList.add(alias);
//    		System.out.println(alias.getId() + " : " + alias.getDestinationAdress() + " " + alias.getSourceAdress());
    	}
    	return aliasList;
    }
    
    public void insertUser(String userName, int domain, String password) throws SQLException {
		// preparedStatements can use variables and are more efficient
		PreparedStatement preparedStatement;
		preparedStatement = connection
				.prepareStatement("insert into  users (username, domain, password) values (?, ?, ?)");
		// parameters start with 1
		preparedStatement.setString(1, userName);
		preparedStatement.setString(2, String.valueOf(domain));
		preparedStatement.setString(3, password);
		preparedStatement.executeUpdate();
    }
    
    public void deleteUserById(int id) throws SQLException {
    	PreparedStatement preparedStatement;
		preparedStatement = connection
				.prepareStatement("DELETE from users where id= ?;");
		// parameters start with 1
		preparedStatement.setString(1, String.valueOf(id));
		preparedStatement.executeUpdate();
    }
    
    public List<Account> getAllUsers() throws SQLException {
    	Statement statement = connection.createStatement();
    	ResultSet rs;
			rs = statement.executeQuery("SELECT * from users;");
    	ArrayList<Account> userList = new ArrayList<Account>();
    	while (rs.next()){
    		Account user = new Account();
//    		user.setUserName(rs.getString("username"));
    		user.setDomain(rs.getString("domain"));
    		user.setPassword(rs.getString("password"));
    		user.setId(Integer.parseInt(rs.getString("id")));
    		userList.add(user);
//    		System.out.println(user.getId() + " " + user.getUserName() + " :" + user.getDomain() + " : " + user.getPassword());
    	}
    	return userList;
    	
    }
}
